from urllib.request import urlretrieve
from time import sleep
import zipfile
import shutil
import os

# SOME VARIABLES:
LAUNCHER_VERSION = "1.0.0"

LICENSE = """    Portable LIKO-12 - Launcher for install and run LIKO-12 in portable form
    Copyright (C) 2019  Ragnar Whitewolf

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>."""

INIT_TEXT = """@###########################@
# PORTABLE LIKO-12 LAUNCHER #
@###########################@
#       VERSION: """+LAUNCHER_VERSION+"""      #
@###########################@
"""

# PATHS CONFIG:
DATAPATH = os.popen("echo %APPDATA%\\LOVE\\LIKO-12\\").read().replace("\n","") # PATH FOR LIKO-12 DATA
LIKOPATH = "LIKO-12" # PATH FOR LIKO-12
USERPATH = "USERDATA" # PATH FOR USER DATA FOLDER
CWDPATH  = os.getcwd() # CURRENT WORKING DIRECTORY
LOVEPATH = "LOVE2D" # PATH FOR THE LOVE2D BINARY
LOVEVER  = "11.2" # LOVE2D VERSION
LOVEARCH = "win32" # LOVE2D ARCHITECTURE

# FUNCTIONS:
def executeLIKO12():
    print("[ OK ] LIKO-12 EXECUTED!\n")
    COMMAND = LOVEPATH+"\\love.exe "+LIKOPATH
    os.system(COMMAND)
    return None

def saveData():
    print("[WARN] SAVING LIKO-12 USER DATA")
    shutil.rmtree(USERPATH)
    shutil.copytree(DATAPATH, USERPATH)
    shutil.rmtree(DATAPATH)
    print("[ OK ] USER DATA SAVED!")
    return None

def loadData():
    try:
        os.chdir(USERPATH)
        os.chdir(CWDPATH)
        print("[WARN] LOADING USER DATA")
        shutil.copytree(USERPATH, DATAPATH)
        print("[ OK ] USERDATA LOADED!\n")
        return None
    except FileNotFoundError:
        os.mkdir(USERPATH)
        return None

# SETUP:
print(INIT_TEXT)
# PATH CHECK:
print("[WARN] EXECUTING FILE CHECK")
try: # CHECK THE LOVE2D DATA PATH
    os.chdir(DATAPATH.replace("\\LIKO-12",""))
    os.chdir(CWDPATH)
    print("  [ OK ] LOVE2D DATA PATH ARE OK!\n")
except FileNotFoundError:
    os.mkdir(DATAPATH.replace("\\LIKO-12",""))
    print("  [WARN] NO LOVE2D DATAPATH FOUND!")
    print("    [ OK ] LOVE2D FOLDER CREATED!\n")

try: # CHECK THE LIKO-12 PATH
    os.chdir(LIKOPATH)
    os.chdir(CWDPATH)
    print("  [ OK ] LIKO-12 FOLDER ARE OK!\n")
except FileNotFoundError:
    print("  [WARN] LIKO-12 NOT FOUND!")
    print("    [WARN] DOWNLOADING LIKO-12")
    urlretrieve("https://github.com/LIKO-12/LIKO-12/archive/master.zip", "LIKO-12.zip")
    print("    [ OK ] LIKO-12 DOWNLOADED!")
    print("    [WARN] EXTRACTING LIKO-12")
    with zipfile.ZipFile("LIKO-12.zip","r") as zip_ref:
        zip_ref.extractall(CWDPATH)
    os.rename("LIKO-12-master",LIKOPATH)
    os.remove("LIKO-12.zip")
    print("    [ OK ] LIKO-12 EXTRACTED!\n")

try: # CHECK FOR LOVE2D
    os.chdir(LOVEPATH)
    os.chdir(CWDPATH)
    print("  [ OK ] LOVE2D ARE OK!\n")
except FileNotFoundError:
    print("  [WARN] LOVE2D NOT FOUND!")
    print("    [WARN] DOWNLOADING LOVE2D")
    urlretrieve("https://bitbucket.org/rude/love/downloads/love-"+LOVEVER+"-"+LOVEARCH+".zip", "LOVE2D.zip")
    print("    [ OK ] LOVE2D DOWNLOADED!")
    print("    [WARN] EXTRACTING LOVE2D")
    with zipfile.ZipFile("LOVE2D.zip", "r") as zip_ref:
        zip_ref.extractall(CWDPATH)
    os.rename("love-"+LOVEVER+".0-"+LOVEARCH,LOVEPATH)
    os.remove("LOVE2D.zip")
    print("    [ OK ] LOVE2D EXTRACTED!\n")

# START THE LIKO-12 PROCCESS:
loadData()
executeLIKO12()
saveData()